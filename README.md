# flying_wolf

BlueStacks App assignment

## [Apk](https://drive.google.com/file/d/16QRLg5ZSiJKhg1xfW9D9O9BvtMIYowre/view?usp=sharing)

## Built With

* [Flutter](https://flutter.dev) - Cross Platform App Development Framework

## How to use this App.
* Just use following password for logging in the app -
* User 1: 9898989898 / password12
* User 2: 9876543210 / password12

## Features Implemented
* Splash
* Login
* Dashboard
* Logout
* Change Language
* Pagination

## Screenshots

![Splash](screenshots/splash_screen.png){height=300 width=150}
![Login](screenshots/login_screen.png" alt="Login){height=300 width=150}
![Login with Id](screenshots/login_screen_with_id.png){height=300 width=150}
![Login with id and password](screenshots/login_screen_with_id_password.png){height=300 width=150}
<br/>

![Loading after Login](screenshots/loading_screen_after_succesful_login.png){height=300 width=150}
![Loading in Japanese](screenshots/login_screen_japanese_locale){height=300 width=150}
![Dashboard](screenshots/dashboard_screen_loading_tournaments.png){height=300 width=150}
![Navigation Drawer](screenshots/dashboard_screen_navigation_drawer.png){height=300 width=150}
<br/>

![Change Language](screenshots/dashboard_screen_change_language.png){height=300 width=150}
![Pagination](screenshots/dashboard_screen_pagination.png){height=300 width=150}
![Dashboard Japanese](screenshots/dashboard_screen_loading_tournaments_japanese_locale.png){height=300 width=150}
<br/>

### Assignment By

# [Harsh Sharma](http://bit.ly/githarsh)

Senior Android Developer

<a href="http://bit.ly/stackharsh"><img src="https://github.com/aritraroy/social-icons/blob/master/stackoverflow-icon.png?raw=true" width="60"></a>
<a href="http://bit.ly/lnkdharsh"><img src="https://github.com/aritraroy/social-icons/blob/master/linkedin-icon.png?raw=true" width="60"></a>
<a href="http://bit.ly/harshfb"><img src="https://github.com/aritraroy/social-icons/blob/master/facebook-icon.png?raw=true" width="60"></a>
