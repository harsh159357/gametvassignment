import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flying_wolf/customviews/background_image.dart';
import 'package:flying_wolf/pages/login/login_button.dart';
import 'package:flying_wolf/utils/constants.dart';
import 'package:flying_wolf/utils/localization/localization.dart';
import 'package:flying_wolf/utils/style_guide/style_export.dart';
import 'package:flying_wolf/wrapper/wrapper_bloc.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';

const Duration kExpand = Duration(milliseconds: 200);

class AuthenticateBasePage extends StatelessWidget implements WrapperState {
  final TextEditingController userIdController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();
  final ValueNotifier<int> userIdStatus = new ValueNotifier(0);
  final ValueNotifier<int> passwordStatus = new ValueNotifier(0);
  final ValueNotifier<bool> formStatus = new ValueNotifier(true);
  final String errorText = LocalizationConstants
      .ENTRY_SHOULD_BE_OF_MIN_3_CHARACTERS_AND_MAX_10
      .localized();

  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
        child: Stack(
      children: [
        BackgroundImage(AppAssets.CALL_OF_DUTY),
        SafeArea(
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: ListView(
              shrinkWrap: true,
              children: [
                AspectRatio(
                  aspectRatio: 16 / 9,
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: SvgPicture.asset(AppAssets.GAME_TV_LOGO),
                  ),
                ),
                SizedBox(
                  height: 100,
                ),
                _numberField(context),
                _passwordField(context),
                SizedBox(
                  height: 50,
                ),
                _validateCredentials(),
                _formStatusWidget(context),
              ],
            ),
          ),
        )
      ],
    ));
  }

  Widget _formStatusWidget(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: formStatus,
      builder: (context, value, child) {
        return !value
            ? Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Center(
                    child: Text(
                      LocalizationConstants.INVALID_USER_ID_OR_PASSWORD
                          .localized(),
                      style: Theme.of(context).textTheme.getContentStyle(
                          color: Theme.of(context).accentColor, size: 14),
                    ),
                  ),
                ),
              )
            : Container();
      },
    );
  }

  Widget _numberField(BuildContext context) {
    return ValueListenableBuilder<int>(
      valueListenable: userIdStatus,
      builder: (context, value, child) {
        String _errorText;
        if (value == 1) {
          _errorText = errorText;
        }

        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 8.0),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.grey[600].withOpacity(0.5),
                borderRadius: BorderRadius.circular(16)),
            child: TextField(
              controller: userIdController,
              keyboardType: TextInputType.number,
              style: inputStyle(context),
              decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(
                      vertical: 16.0, horizontal: 16.0),
                  border: InputBorder.none,
                  prefixIcon: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Icon(
                      Icons.account_circle,
                      color: Colors.white,
                    ),
                  ),
                  hintText: LocalizationConstants.USER_ID.localized(),
                  hintStyle: hintStyle(context),
                  errorText: _errorText,
                  errorStyle: errorStyle(context),
                  errorMaxLines: 2),
              onChanged: (entry) => _validateEntry(entry, userIdStatus),
            ),
          ),
        );
      },
    );
  }

  Widget _passwordField(BuildContext context) {
    return ValueListenableBuilder<int>(
        valueListenable: passwordStatus,
        builder: (context, value, child) {
          String _errorText;
          if (value == 1) {
            _errorText = errorText;
          }
          return Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 32.0, vertical: 8.0),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.grey[600].withOpacity(0.5),
                  borderRadius: BorderRadius.circular(16)),
              child: TextField(
                  obscureText: true,
                  controller: passwordController,
                  style: inputStyle(context),
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 16.0, horizontal: 16),
                    border: InputBorder.none,
                    prefixIcon: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Icon(
                        Icons.vpn_key,
                        color: Colors.white,
                      ),
                    ),
                    hintText: LocalizationConstants.PASSWORD.localized(),
                    hintStyle: hintStyle(context),
                    errorText: _errorText,
                    errorStyle: errorStyle(context),
                    errorMaxLines: 2,
                  ),
                  onChanged: (entry) => _validateEntry(entry, passwordStatus)),
            ),
          );
        });
  }

  void _validateEntry(String entry, ValueNotifier<int> notifier) {
    if (entry.isEmpty) {
      notifier.value = 0;
      return;
    }

    if (entry.length < 3 || entry.length > 10) {
      notifier.value = 1;
      return;
    }
    notifier.value = 2;
    return;
  }

  Widget _validateCredentials() {
    return LoginButton(
      passwordStatus: passwordStatus,
      userIdStatus: userIdStatus,
      userId: userIdController.text,
      password: passwordController.text,
      loginFormStatus: formStatus,
    );
  }

  TextStyle errorStyle(BuildContext context) => Theme.of(context)
      .textTheme
      .getSubHeaderStyle(color: Colors.red.shade100, size: 12);

  TextStyle hintStyle(BuildContext context) => Theme.of(context)
      .textTheme
      .getContentStyle(color: Theme.of(context).hintColor, size: 16);

  TextStyle inputStyle(BuildContext context) => Theme.of(context)
      .textTheme
      .getContentStyle(color: Theme.of(context).cardColor, size: 16);
}
