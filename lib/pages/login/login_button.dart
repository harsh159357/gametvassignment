import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flying_wolf/utils/constants.dart';
import 'package:flying_wolf/utils/localization/localization.dart';
import 'package:flying_wolf/utils/style_guide/style_export.dart';
import 'package:flying_wolf/wrapper/wrapper_bloc.dart';

class LoginButton extends StatelessWidget {
  final ValueNotifier<int> userIdStatus;
  final ValueNotifier<int> passwordStatus;
  final ValueNotifier<bool> loginFormStatus;
  final String userId;
  final String password;

  LoginButton(
      {@required this.passwordStatus,
      @required this.userIdStatus,
      @required this.userId,
      @required this.password,
      @required this.loginFormStatus});

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<int>(
        valueListenable: userIdStatus,
        builder: (context, _uidStatus, child) {
          return ValueListenableBuilder<int>(
              valueListenable: passwordStatus,
              builder: (context, _pwdStatus, child) {
                bool isClickable = (_uidStatus + _pwdStatus) == 4;

                double _opacity = isClickable ? 1 : 0.3;

                return IgnorePointer(
                  ignoring: !isClickable,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 55),
                    child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          color: Theme.of(context)
                              .accentColor
                              .withOpacity(_opacity),
                          borderRadius: BorderRadius.circular(16)),
                      child: FlatButton(
                        onPressed: () {
                          loginFormStatus.value =
                              BlocProvider.of<WrapperCubit>(context)
                                  .verifyUserCred(userId, password);
                          if (loginFormStatus.value) {
                            BlocProvider.of<WrapperCubit>(context)
                                .loginUser(userId);
                          }
                        },
                        child: Center(
                          child: Text(
                            LocalizationConstants.LOGIN.localized(),
                            style: Theme.of(context)
                                .textTheme
                                .getSubHeaderStyle(
                                    color: Theme.of(context)
                                        .cardColor
                                        .withOpacity(_opacity),
                                    size: 16),
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              });
        });
  }
}
