import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flying_wolf/models/user.dart';
import 'package:flying_wolf/pages/dashboard/navigation_drawer.dart';
import 'package:flying_wolf/pages/dashboard/tournament_list/tournament_list.dart';
import 'package:flying_wolf/pages/dashboard/tournament_list/tournament_list_bloc.dart';
import 'package:flying_wolf/pages/dashboard/user_info.dart';
import 'package:flying_wolf/utils/constants.dart';
import 'package:flying_wolf/utils/localization/localization.dart';
import 'package:flying_wolf/utils/style_guide/style_export.dart';
import 'package:flying_wolf/wrapper/wrapper_bloc.dart';

class Dashboard extends StatelessWidget implements WrapperState {
  final User user;

  Dashboard(this.user);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<TournamentListBloc>(
      create: (context) => TournamentListBloc(),
      child: DashboardChannel(user),
    );
  }
}

class DashboardChannel extends StatefulWidget {
  final User user;

  DashboardChannel(this.user);

  @override
  _DashboardChannelState createState() => _DashboardChannelState();
}

class _DashboardChannelState extends State<DashboardChannel> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();

  TournamentListBloc tBloc;

  bool isLoading = false;

  @override
  void initState() {
    tBloc = BlocProvider.of<TournamentListBloc>(context);
    tBloc.add(TournamentListEvent.Initiate);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    tBloc = BlocProvider.of<TournamentListBloc>(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Theme
            .of(context)
            .backgroundColor,
        drawer: Drawer(
          child: NavigationDrawer(),
        ),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          centerTitle: true,
          title: _title(context),
          leading: _leading(context),
          actions: [
            _actionLogout(context),
          ],
        ),
        body: BlocListener<TournamentListBloc, int>(
          listener: (context, state) {
            if (state > -2) {
              setState(() {
                isLoading = false;
              });
            }
          },
          child: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () => _refresh(context),
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (!isLoading &&
                    scrollInfo.metrics.pixels ==
                        scrollInfo.metrics.maxScrollExtent) {
                  setState(() {
                    isLoading = true;
                  });
                  _triggerPagination(context);
                }
                return;
              },
              child: CustomScrollView(
                slivers: [userInfo(context, widget.user), TournamentList()],
              ),
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            showModalBottomSheet(
                context: context,
                builder: (context2) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new ListTile(
                        title: new Text(english),
                        onTap: () {
                          BlocProvider.of<WrapperCubit>(context)
                              .changeLanguage(english);
                          Navigator.pop(context);
                        },
                      ),
                      new ListTile(
                        title: new Text(japanese),
                        onTap: () {
                          BlocProvider.of<WrapperCubit>(context)
                              .changeLanguage(japanese);
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  );
                });
          },
          child: Icon(Icons.language),
        ),
      ),
    );
  }

  Future<void> _refresh(BuildContext context) async =>
      tBloc.add(TournamentListEvent.Refresh);

  _triggerPagination(BuildContext context) =>
      tBloc.add(TournamentListEvent.Load);

  Widget _title(BuildContext context) =>
      Text(widget.user.gamerAlias,
          style: Theme
              .of(context)
              .textTheme
              .getSubHeaderStyle(color: Theme
              .of(context)
              .secondaryHeaderColor));

  Widget _leading(BuildContext context) =>
      FlatButton(
        onPressed: () {
          _scaffoldKey.currentState.openDrawer();
        },
        child: SvgPicture.asset(AppAssets.DRAWER_LOGO),
      );

  Widget _actionLogout(BuildContext context) =>
      IconButton(
        icon: Icon(
          Icons.logout,
          color: Colors.black,
        ),
        onPressed: () {
          BlocProvider.of<WrapperCubit>(context).signOut();
        },
      );
}
