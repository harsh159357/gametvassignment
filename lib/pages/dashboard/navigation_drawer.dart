
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flying_wolf/customviews/background_image.dart';
import 'package:flying_wolf/utils/constants.dart';

class NavigationDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          _gameTvLogo(context),
          Divider(
            height: 1,
            thickness: 1,
            color: Theme.of(context).hintColor,
          ),
        ],
      ),
    );
  }

  Widget _gameTvLogo(BuildContext context) {
    return AspectRatio(
      aspectRatio: 16 / 9,
      child: Container(
          width: MediaQuery.of(context).size.width,
          height: 100,
          child: Stack(
            children: [
              BackgroundImage(AppAssets.DRAWER_BACKGROUND),
              SizedBox.expand(
                child: ShaderMask(
                    shaderCallback: (Rect bounds) {
                      return LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            Theme.of(context).cardColor.withOpacity(0.4),
                            Theme.of(context).cardColor
                          ]).createShader(bounds);
                    },
                    child: SvgPicture.asset(AppAssets.GAME_TV_LOGO)),
              ),
            ],
          )),
    );
  }
}
