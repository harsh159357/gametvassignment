import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flying_wolf/customviews/game_card.dart';
import 'package:flying_wolf/models/tournament.dart';

class TListLoaded extends StatelessWidget {
  final List<Tournament> tournamentList;
  final bool hasEnded;

  TListLoaded(this.tournamentList, this.hasEnded);

  @override
  Widget build(BuildContext context) {
    return SliverList(
      key: UniqueKey(),
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          if (index == tournamentList.length && !hasEnded) {
            return _loader(context);
          }

          return (index < tournamentList.length)
              ? GameCard(tournamentList[index])
              : null;
        },
      ),
    );
  }

  Widget _loader(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 50,
        child: Center(
          child: ShaderMask(
            shaderCallback: (Rect bounds) {
              return LinearGradient(
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  colors: [
                    Theme.of(context).accentColor,
                    Theme.of(context).accentColor.withAlpha(5)
                  ]).createShader(bounds);
            },
            child: SpinKitWave(
              color: Theme.of(context).hintColor,
              size: 25,
            ),
          ),
        ));
  }
}
