import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flying_wolf/pages/dashboard/tournament_list/tournament_list_bloc.dart';
import 'package:flying_wolf/pages/dashboard/tournament_list/tournament_list_states_ui.dart';

class TournamentList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TournamentListBloc, int>(builder: (context, state) {
      return TournamentListStateUI(state);
    });
  }
}
