import 'dart:developer';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flying_wolf/futures/service_locator.dart';
import 'package:flying_wolf/futures/tournament_repository.dart';
import 'package:flying_wolf/models/tournament.dart';
import 'package:flying_wolf/utils/constants.dart';
import 'package:hive/hive.dart';

enum TournamentListEvent { Initiate, Load, Refresh }

class TournamentListBloc extends Bloc<TournamentListEvent, int> {
  List<Tournament> tournamentList = new List();
  String cursor;
  int tListLoadedState;

  Box tournamentBox;

  TournamentListBloc() : super(0) {
    tournamentBox =
        serviceLocator.get<TournamentRepository>().getTournamentBox();
    refreshList();
  }

  @override
  Stream<int> mapEventToState(TournamentListEvent event) async* {
    switch (event) {
      case TournamentListEvent.Initiate:
      case TournamentListEvent.Refresh:
        yield* refreshList();
        break;
      case TournamentListEvent.Load:
        yield* fetchTList();
        break;
    }
  }

  Stream<int> fetchTList() async* {
    String _cursor;

    try {
      _cursor = await serviceLocator
          .get<TournamentRepository>()
          .getTournamentData(cursor);
      tournamentList =
          List<Tournament>.from(tournamentBox.values) ?? <Tournament>[];
    } catch (e) {
      log("$e");
      _cursor = Error.ERROR;
    }

    if (_cursor == Error.ERROR) {
      if (cursor == null) {
        yield -1;
      } else {
        yield 1;
      }
    } else {
      cursor = _cursor;
      if (tournamentList.length == 0) {
        yield 0;
      } else {
        if (_cursor == null) {
          yield 1;
        } else {
          yield tListLoadedState++;
        }
      }
    }

    log("${LogMsg.TOURNAMENT_COUNT} ${tournamentList.length}");
  }

  Stream<int> refreshList() async* {
    yield -2;
    tListLoadedState = 2;
    tournamentList.clear();
    if (tournamentBox.isNotEmpty) {
      tournamentBox.clear();
    }
    cursor = null;
    yield* fetchTList();
  }
}
