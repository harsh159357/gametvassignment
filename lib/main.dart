import 'package:flutter/material.dart';
import 'package:flying_wolf/utils/style_guide/theme_utils.dart';
import 'package:flying_wolf/wrapper/wrapper.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flying Wolf',
      theme: baseLightTheme(),
      home: Wrapper(),
      debugShowCheckedModeBanner: false,
    );
  }
}
