import 'package:flutter/material.dart';

ThemeData baseLightTheme() {
  final ThemeData base = ThemeData.light();

  return base.copyWith(
    primaryColor: Color(0xff53bfb2),
    accentColor: Color(0xffe30b4f),
    hintColor: Colors.grey,
    backgroundColor: Colors.grey.shade300,
    cardColor: Colors.white,
    secondaryHeaderColor: Colors.black,
  );
}
