import 'package:flying_wolf/utils/constants.dart';
import 'package:hive/hive.dart';

const String languagePreference = "languagePreference";
const String english = "English";
const String japanese = "Japanese";

final Map<String, String> en = {
  LocalizationConstants.PREFERRED_LANGUAGE: "Preferred Language",
  LocalizationConstants.SELECT_LANGUAGE: "Select Language",
  LocalizationConstants.SIGN_OUT: "Sign Out",
  LocalizationConstants.RECOMMENDED_FOR_YOU: "Recommended for you",
  LocalizationConstants.ELO_RATING: "ELO Rating",
  LocalizationConstants.TOURNAMENTS_PLAYED: "Tournaments\nPlayed",

  LocalizationConstants.TOURNAMENTS_WON: "Tournaments\nWon",
  LocalizationConstants.WINNING_PERCENTAGE: "Winning\nPercentage",
  LocalizationConstants.ERROR_IN_LOADING_TOURNAMENTS:
      "Error in loading tournaments.\nPlease try after changing your network connection.",
  LocalizationConstants.TOURNAMENTS_LIST_EMPTY: "Tournaments list empty",
  LocalizationConstants.LOADING_TOURNAMENTS: "Loading Tournaments",
  LocalizationConstants.ENTRY_SHOULD_BE_OF_MIN_3_CHARACTERS_AND_MAX_10:
      "Entry should be of min 3 characters and max 10.",
  LocalizationConstants.INVALID_USER_ID_OR_PASSWORD:
      "Invalid User Id or Password.",
  LocalizationConstants.USER_ID: "User Id",
  LocalizationConstants.PASSWORD: "Password",
  LocalizationConstants.LOADING_IN: "Loading In",
  LocalizationConstants.LOADING_USER: "Loading User",
  LocalizationConstants.LOGIN: "LOGIN"
};

final Map<String, String> jap = {
  LocalizationConstants.PREFERRED_LANGUAGE: "優先言語",
  LocalizationConstants.SELECT_LANGUAGE: "言語を選択する",
  LocalizationConstants.SIGN_OUT: "ログアウト",
  LocalizationConstants.RECOMMENDED_FOR_YOU: "あなたにおすすめ",
  LocalizationConstants.ELO_RATING: "ELO 評価",
  LocalizationConstants.TOURNAMENTS_PLAYED: "プレーした\nトーナメント",
  LocalizationConstants.TOURNAMENTS_WON: "トーナメント\n優勝",
  LocalizationConstants.WINNING_PERCENTAGE: "勝率\n",
  LocalizationConstants.ERROR_IN_LOADING_TOURNAMENTS:
      "トーナメントの読み込み中にエラーが発生しました。\nネットワーク接続を変更してからお試しください。",
  LocalizationConstants.TOURNAMENTS_LIST_EMPTY: "トーナメントリストが空です",
  LocalizationConstants.LOADING_TOURNAMENTS: "トーナメントの読み込み",
  LocalizationConstants.ENTRY_SHOULD_BE_OF_MIN_3_CHARACTERS_AND_MAX_10:
      "エントリは最小3文字、最大10文字である必要があります。",
  LocalizationConstants.INVALID_USER_ID_OR_PASSWORD: "無効なユーザーIDまたはパスワード。",
  LocalizationConstants.USER_ID: "ユーザーID",
  LocalizationConstants.PASSWORD: "パスワード",
  LocalizationConstants.LOADING_IN: "読み込み中",
  LocalizationConstants.LOADING_USER: "ユーザーの読み込み",
  LocalizationConstants.LOGIN: "ログインする"
};

final Map<String, Map<String, String>> languageMap = {
  english: en,
  japanese: jap
};

extension Localize on String {
  String localized() {
    String _languagePreference = Hive.box<String>(languagePreference).getAt(0);

    String result = languageMap[_languagePreference ?? english][this];

    return result ?? this;
  }
}
