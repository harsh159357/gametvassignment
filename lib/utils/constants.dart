class ApiConstants {
  static const String USER_API_URL =
      "https://run.mocky.io/v3/7d40af41-0805-4e36-bb9b-aab3892f613f";
  static const String TOURNAMENT_API_URL =
      "https://tournaments-dot-game-tv-prod.uc.r.appspot.com/tournament/api/tournaments_list_v2?limit=10&status=all";
}

class Error {
  static const String ERROR = "Error";
  static const String JSON_DECODING_ERROR_USER = "Json Decoding Error User - ";
  static const String RESPONSE_ERROR_USER = "Response Error User - ";
  static const String JSON_DECODING_ERROR_TOURNAMENT =
      "Json Decoding Error Tournament - ";
  static const String RESPONSE_ERROR_TOURNAMENT =
      "Response Error Tournament - ";
  static const String USER_ADDING_ERROR_HIVE = "User adding error hive - ";
  static const String LOADING_TOURNAMENT_ERROR =
      "Error in Loading Tournament - ";
  static const String FETCHING_TOURNAMENTS_ERROR =
      "Fetching Tournament Error  - ";
}

class LogMsg {
  static const String TOURNAMENT_COUNT = "Tournament Count";
}

class LocalizationConstants {
  static const String PREFERRED_LANGUAGE = "PREFERRED_LANGUAGE";
  static const String SELECT_LANGUAGE = "SELECT_LANGUAGE";
  static const String SIGN_OUT = "SIGN_OUT";
  static const String RECOMMENDED_FOR_YOU = "RECOMMENDED_FOR_YOU";
  static const String ELO_RATING = "ELO_RATING";
  static const String TOURNAMENTS_PLAYED = "TOURNAMENTS_PLAYED";
  static const String TOURNAMENTS_WON = "TOURNAMENTS_WON";
  static const String WINNING_PERCENTAGE = "WINNING_PERCENTAGE";
  static const String ERROR_IN_LOADING_TOURNAMENTS =
      "ERROR_IN_LOADING_TOURNAMENTS";
  static const String TOURNAMENTS_LIST_EMPTY = "TOURNAMENTS_LIST_EMPTY";
  static const String LOADING_TOURNAMENTS = "LOADING_TOURNAMENTS";
  static const String ENTRY_SHOULD_BE_OF_MIN_3_CHARACTERS_AND_MAX_10 =
      "ENTRY_SHOULD_BE_OF_MIN_3_CHARACTERS_AND_MAX_10";
  static const String INVALID_USER_ID_OR_PASSWORD =
      "INVALID_USER_ID_OR_PASSWORD";
  static const String USER_ID = "USER_ID";
  static const String PASSWORD = "PASSWORD";
  static const String LOADING_IN = "LOADING_IN";
  static const String LOADING_USER = "LOADING_USER";
  static const String LOGIN = "LOGIN";
}

class AppAssets {
  static const String GAME_TV_LOGO = "assets/gametvlogo.svg";
  static const String CALL_OF_DUTY = "assets/callofduty.jpg";
  static const String DRAWER_BACKGROUND = "assets/drawerbackground.jpg";
  static const String LOGGING_IN = "assets/loggingin.jpg";
  static const String DRAWER_LOGO = "assets/drawerlogo.svg";
  static const String PROFILE_PIC = "assets/profilepic.jpg";
  static const String GHOST = "assets/ghost.svg";
}
