import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flying_wolf/customviews/loading_placeholder.dart';
import 'package:flying_wolf/futures/service_locator.dart';
import 'package:flying_wolf/futures/user_repository.dart';
import 'package:flying_wolf/models/user.dart';
import 'package:flying_wolf/pages/dashboard/dashboard_page.dart';
import 'package:flying_wolf/pages/login/login_page.dart';
import 'package:flying_wolf/utils/constants.dart';
import 'package:flying_wolf/utils/localization/localization.dart';
import 'package:hive/hive.dart';

abstract class WrapperState {}

class WrapperCubit extends Cubit<WrapperState> {
  User user;
  Box<User> _userBox;

  WrapperCubit()
      : super(LoadingPlaceHolder(
          svgPath: AppAssets.GAME_TV_LOGO,
        )) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    initializeApp();
  }

  Future<void> initializeApp() async {
    setUpServices();
    await setUpHive();
    _userBox = serviceLocator.get<UserRepository>().getUserBox();
    checkIfUserExists();
  }

  checkIfUserExists() async {
    if (_userBox.values.isEmpty) {
      emit(AuthenticateBasePage());
    } else {
      getInitialState();
    }
  }

  getInitialState() {
    user = _userBox.getAt(0);
    emit(Dashboard(user));
  }

  bool verifyUserCred(String uid, String pwd) =>
      serviceLocator.get<UserRepository>().verifyUserCred(uid, pwd);

  Future<bool> loginUser(String uid) async {
    emit(LoadingPlaceHolder(
      imagePath: AppAssets.LOGGING_IN,
      message: LocalizationConstants.LOADING_USER.localized(),
    ));
    await serviceLocator.get<UserRepository>().loadUserCredentials(uid);
    checkIfUserExists();
  }

  Future<void> signOut() async {
    await serviceLocator.get<UserRepository>().signOut();
    checkIfUserExists();
  }

  changeLanguage(String language) async {
    emit(LoadingPlaceHolder(
      imagePath: AppAssets.LOGGING_IN,
      message: LocalizationConstants.LOADING_IN.localized() + " $language",
    ));
    await Hive.box<String>(languagePreference).clear();
    await Hive.box<String>(languagePreference).add(language);
    await Future.delayed(Duration(seconds: 1));
    checkIfUserExists();
  }
}
