import 'dart:developer';

import 'package:flying_wolf/futures/network_service.dart';
import 'package:flying_wolf/futures/service_locator.dart';
import 'package:flying_wolf/models/tournament.dart';
import 'package:flying_wolf/utils/constants.dart';
import 'package:flying_wolf/utils/pair.dart';
import 'package:hive/hive.dart';

class TournamentRepository {
  Box<Tournament> getTournamentBox() => Hive.box(tournamentBox);

  Future<String> getTournamentData([String cursor]) async {
    Pair<List<Map<String, dynamic>>, String> response =
        await serviceLocator.get<NetworkService>().fetchTournamentData(cursor);

    if (response == null) return Error.FETCHING_TOURNAMENTS_ERROR;

    log("${LogMsg.TOURNAMENT_COUNT} ${response.first?.length}");

    Box<Tournament> tournamentBox = getTournamentBox();

    response?.first?.forEach((element) {
      try {
        tournamentBox.add(Tournament.fromMap(element));
      } catch (e) {
        log("${Error.LOADING_TOURNAMENT_ERROR} ${element.toString()} \n $e");
      }
    });

    log("${LogMsg.TOURNAMENT_COUNT}${tournamentBox.values.length}");

    return response?.second;
  }
}
