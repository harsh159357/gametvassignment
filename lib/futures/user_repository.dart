import 'dart:developer';

import 'package:flying_wolf/futures/network_service.dart';
import 'package:flying_wolf/futures/service_locator.dart';
import 'package:flying_wolf/models/user.dart';
import 'package:flying_wolf/utils/constants.dart';
import 'package:hive/hive.dart';

import '../utils/credentials.dart';

class UserRepository {
  final NetworkService networkService = serviceLocator.get<NetworkService>();

  Box<User> getUserBox() => Hive.box(userBox);

  bool verifyUserCred(String uid, String pwd) {
    if (!credentials.containsKey(uid)) {
      return false;
    }

    return credentials[uid] == pwd;
  }

  Future<void> loadUserCredentials(String uid) async {
    List<Map<String, dynamic>> response = await networkService.fetchUserData();
    Map<String, dynamic> data =
        response?.firstWhere((element) => element["uid"] == uid);
    return addUsersToHive(data);
  }

  Future<void> addUsersToHive(Map<String, dynamic> data) async {
    var _userBox = getUserBox();
    try {
      await _userBox.clear();
      User _user = User.fromMap(data);
      await _userBox.add(_user);
    } catch (e) {
      log(Error.USER_ADDING_ERROR_HIVE + e.toString());
    }
  }

  signOut() async => await getUserBox().clear();
}
