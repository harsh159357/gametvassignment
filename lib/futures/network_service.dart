import 'dart:convert';
import 'dart:developer';

import 'package:flying_wolf/utils/constants.dart';
import 'package:flying_wolf/utils/pair.dart';
import 'package:http/http.dart' as http;

class NetworkService {
  Future<List<Map<String, dynamic>>> fetchUserData() async {
    final response = await http.get(ApiConstants.USER_API_URL);
    List<Map<String, dynamic>> result;
    if (response.statusCode == 200) {
      try {
        result = List<Map<String, dynamic>>.from(jsonDecode(response.body));
      } catch (e) {
        log(Error.JSON_DECODING_ERROR_USER + e.toString());
      }
    } else {
      log("${Error.RESPONSE_ERROR_USER}${response.statusCode}");
    }

    return result;
  }

  Future<Pair<List<Map<String, dynamic>>, String>> fetchTournamentData(
      [String cursor]) async {
    String _api = ApiConstants.TOURNAMENT_API_URL +
        ((cursor != null) ? "&cursor=$cursor" : "");

    final response = await http.get(_api);
    List<Map<String, dynamic>> _tList;
    String _cursor;
    if (response.statusCode == 200) {
      try {
        Map<String, dynamic> result = jsonDecode(response.body);
        _tList = List<Map<String, dynamic>>.from(result["data"]["tournaments"]);
        _cursor = result["data"]["cursor"];
        return Pair(_tList, _cursor);
      } catch (e) {
        log(Error.JSON_DECODING_ERROR_TOURNAMENT + e.toString());
      }
    } else {
      log("${Error.RESPONSE_ERROR_TOURNAMENT}${response.statusCode} ");
    }
    return null;
  }
}
