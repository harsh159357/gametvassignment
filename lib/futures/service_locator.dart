import 'package:flying_wolf/futures/network_service.dart';
import 'package:flying_wolf/futures/tournament_repository.dart';
import 'package:flying_wolf/futures/user_repository.dart';
import 'package:flying_wolf/models/tournament.dart';
import 'package:flying_wolf/models/user.dart';
import 'package:flying_wolf/utils/localization/localization.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as path;

GetIt serviceLocator = GetIt.instance;

void setUpServices() {
  serviceLocator.registerLazySingleton<UserRepository>(() => UserRepository());
  serviceLocator.registerLazySingleton<TournamentRepository>(
      () => TournamentRepository());
  serviceLocator.registerLazySingleton<NetworkService>(() => NetworkService());
}

setUpHive() async {
  final appDocDirectory = await path.getApplicationDocumentsDirectory();
  Hive.init(appDocDirectory.path);
  Hive.registerAdapter(UserAdapter());
  Hive.registerAdapter(TournamentAdapter());

  if (!Hive.isBoxOpen(userBox)) await Hive.openBox<User>(userBox);

  if (!Hive.isBoxOpen(tournamentBox))
    await Hive.openBox<Tournament>(tournamentBox);

  if (!Hive.isBoxOpen(languagePreference))
    await Hive.openBox<String>(languagePreference);

  if (Hive.box<String>(languagePreference).isEmpty) {
    await Hive.box<String>(languagePreference).add(english);
  }
}
